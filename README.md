Deutsche Übersetzung für mono_garage
=====================================

V1.2.2
------

<h1>Anleitung:</h1>
Fügen Sie vom Verzeichnis **locales/de.json** in ihr mono_garage-Verzeichnis.

<h2>Aktivierung:</h2>
Stellen Sie sicher das die Deutsche Übersetzung aktiv ist, indem Sie das "en.json" umbennen in "en.json_old" und das "de.json" in "en.json"
